#+html: <a href="http://www.gnu.org/licenses/agpl-3.0.html"> <img src="https://img.shields.io/badge/License-AGPL--3.0-blue.svg" alt="License: AGPL-3.0" /> </a>
#+html: <a href="https://gitlab.com/flattrack/flattrack/releases"> <img src="https://img.shields.io/badge/version-0.0.1--alpha11-1--brightgreen.svg" alt="Version 0.0.1-alpha11-1" /> </a>
#+html: <a href='https://ind.ie/ethical-design'> <img style='margin-left: auto; margin-right: auto;' alt='We practice Ethical Design' src='https://img.shields.io/badge/Ethical_Design-_▲_❤_-blue.svg'> </a>
#+html: <br/>

* FlatTrack
#+begin_quote
Collaborate with your flatmates
#+end_quote

** Features
- Shopping List
- Tasks (WIP - no progress)
- Noticeboard (WIP - no progress)
- Shared Calendar (WIP - no progress)
- Recipes (WIP - no progress)
- Flatmates (WIP - no progress)
- Highfives (WIP - no progress)

** Technologies
- [[https://golang.org][golang]] - backend
- [[https://vuejs.org][vuejs]] - frontend
- [[https://github.com/gorilla/mux][gorilla/mux]] - HTTP multiplexer
- [[https://buefy.org][bulma]] + [[https://buefy.org][buefy]] - CSS framework
- [[http://vuematerial.io][vuematerial]] - CSS framework
- [[https://github.com/axios/axios][axios]] - client-side HTTP request library
- [[https://onsi.github.io/ginkgo][ginkgo]] & [[https://onsi.github.io/ginkgo][gomega]] - tests

** Getting started
Various options are available for running a FlatTrack instance:
- [[https://flattrack.io][FlatTrack.io hosting]] (recommended; coming soon)
- [[./docs/deployment.org][Self-hosted Kubernetes]] (recommended)
- [[./docs/deployment.org][Self-hosted Docker-Compose]]
- [[./docs/deployment.org][Self-hosted plain Ubuntu server]]

** Documentation
To view the documentation, please check out the GitLab-hosted [[https://flattrack.gitlab.io/flattrack][FlatTrack docs]]

** Contribution
*** Development
From code, to assets/artwork, to community, to documentation, there are many ways to contribute.  
To learn how to contribute, please refer to the [[./docs/development.org][development+contribution documentation]].
Looking for something to do? Check out the [[https://gitlab.com/flattrack/flattrack/-/issues][FlatTrack issues]] page.

** Community
Join FlatTrack's community to chip in and improve it!  
Please read:
- [[docs/contributing.org][contributing docs]].
- [[docs/community.org][community docs]].

** License
Copyright 2019-2020 Caleb Woodbine.  
This project is licensed under the [[http://www.gnu.org/licenses/agpl-3.0.html][AGPL-3.0]] and is [[https://www.gnu.org/philosophy/free-sw.en.html][Free Software]].
This program comes with absolutely no warranty.
